import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
class BankOperations{
    private static ArrayList<BankClient> clients = new ArrayList<BankClient>();
    private static ArrayList<BankClient> filteredClients = new ArrayList<BankClient>();
    public static void addTestClient(){
            BankClient cleint = new BankClient("Michal","Kichal","12345678910","Ladna");
            addClient(cleint);
    }
    public static void addClient(){
        String name = Utils.getUserString("Enter client name");
        String surname = Utils.getUserString("Enter client surname");
        String pesel = Utils.getPesel("Enter client pesel");
        String address = Utils.getUserLongText("Enter client address");
        if(Utils.getConfirmation("Add client (Name: "+name+", Surname: "+surname+", Pesel: "+pesel+", Adress: "+address+")")){
            BankClient cleint = new BankClient(name,surname,pesel,address);
            addClient(cleint);
        }
    }
    private static void addClient(BankClient client){
        clients.add(client);
        Collections.sort(clients,new Comparator<BankClient>() {
            @Override
            public int compare(BankClient c1, BankClient c2){
                return c1.getId() - c2.getId();
            }
        });
        BankOperations.save();
    }
    public static void showAllClients(){
        if(clients.size() == 0){
            System.out.println("There are no clients in database");
            return;
        }
        System.out.println(Config.getTableHeader());
        clients.forEach((client)->{
            System.out.println(Config.getDelimiter());
            System.out.println(client);
        });
    }
    public static void showFiltredClients(){
        if(filteredClients.size() == 0){
            System.out.println("There are no clients fullfilling given criteria");
            return;
        }
        System.out.println(Config.getTableHeader());
        filteredClients.forEach((client)->{
            System.out.println(Config.getDelimiter());
            System.out.println(client);
        });
    }
    public static void removeClient(){
        BankClient c = null;
        if(clients.size()<1){
            System.out.println("Empty database");
            return;
        }
        if(clients.size() == 1){
            System.out.println("Using only client in database");
            c = clients.get(0);
        } else{
            if(filteredClients.size()==1 && Utils.getConfirmation("Do you want to delete last chosen user \n"+filteredClients.get(0)+"\n")){
                c = filteredClients.get(0);
            } else{
                System.out.println("Choose client to delete");
                BankOperations.filter(1);
                c = filteredClients.get(0);
            }
        }
        if(Utils.getConfirmation("Do you want to delete user \n" + c +"\n")){
            clients.remove(c);
            BankOperations.save();
            filteredClients.clear();
        }
    }
    public static void filter(int expcetedValue){
        if(clients.size()<1){
            System.out.println("Empty database");
            return;
        }
        if(clients.size() == 1){
            System.out.println("There is single client in database");
            filteredClients.add(clients.get(0));
            return;
        }
        Boolean exitValue = true;
        filteredClients.clear();
        filteredClients.addAll(clients);
        do{
            BankOperations.filterMenu();
            char option = Utils.getUserChar("Select filter type");
            switch(option){
                case 'i' :
                    int idFilter = Utils.getUserInt("Enter ID to search for");
                    filteredClients.removeIf((client)->{
                        return client.getId() != idFilter;
                    });
                break;
                case 'n' :
                    String nameFilter = Utils.getUserString("Enter name to search for");
                    filteredClients.removeIf((client)->{
                        return !client.getName().equals(nameFilter);
                    });
                break;
                case 's' :
                    String surnameFilter = Utils.getUserString("Enter surname to search for");
                    filteredClients.removeIf((client)->{
                        return !client.getSurname().equals(surnameFilter);
                    });
                break;
                case 'p' :
                    String peselFilter = Utils.getPesel("Enter pesel to search for");
                    filteredClients.removeIf((client)->{
                        return !client.getPesel().equals(peselFilter);
                    });
                break;
                case 'a' :
                    String addressFilter = Utils.getUserLongText("Enter address to search for");
                    filteredClients.removeIf((client)->{
                        return !client.getAddress().equals(addressFilter);
                    });
                break;
                
                default:
                    System.out.println("No such filter available options are charactes in square brackets");
                break;
            }
            BankOperations.showFiltredClients();
            if(expcetedValue == -1){
                if(filteredClients.size()>1){
                    exitValue = Utils.getConfirmation("Filter more?");
                } else{
                    exitValue = false;
                } 
            } else{
                if(filteredClients.size()>1)
                    System.out.println("You need to choose 1 client");
                if(filteredClients.size()==0){
                    System.out.println("You need to choose 1 client");
                    filteredClients.clear();
                    filteredClients.addAll(clients);
                }
                if(filteredClients.size()==1)
                    exitValue = false;
            }
        }while(exitValue);
    }
    public static void addBalance(){
        BankClient c = null;
        if(clients.size()<1){
            System.out.println("Empty database");
            return;
        }
        if(clients.size() == 1){
            System.out.println("Using only client in database");
            c = clients.get(0);
        } else{
            if(filteredClients.size()==1 && Utils.getConfirmation("Do you want to use last chosen user \n"+filteredClients.get(0)+"\n")){
                c = filteredClients.get(0);
            } else{
                System.out.println("Choose client to add money to");
                BankOperations.filter(1);
                c = filteredClients.get(0);
            }
        }
        double amount = Utils.getUserDouble("Enter amount of money to transact.");
        if(Utils.getConfirmation("Do you want to add "+ amount + " to  client \n" + c +"\n")){
            c.addBalance(amount);
            BankOperations.save();
        }
    }
    public static void subBalance(){
        BankClient c = null;
        if(clients.size()<1){
            System.out.println("Empty database");
            return;
        }
        if(clients.size() == 1){
            System.out.println("Using only client in database");
            c = clients.get(0);
        } else{
            if(filteredClients.size()==1 && Utils.getConfirmation("Do you want to use last chosen user \n"+filteredClients.get(0)+"\n")){
                c = filteredClients.get(0);
            } else{
                System.out.println("Choose client to subtract money from");
                BankOperations.filter(1);
                c = filteredClients.get(0);
            }
        }
        double amount = Utils.getUserDouble("Enter amount of money to transact.");
        if(Utils.getConfirmation("Do you want to subtract "+ amount + " from  client \n" + c +"\n")){
            c.subBalance(amount);
            BankOperations.save();
        }
    }
    public static void transfer(){
        if(clients.size()<2){
            System.out.println("Not enough clients to ");
            return;
        }
        BankClient c = null;
        BankClient c1 = null;
        if(filteredClients.size()==1 && Utils.getConfirmation("Do you want to use last chosen user \n"+filteredClients.get(0)+"\n")){
            if(Utils.getConfirmation("Use this user as money receiver \n"+filteredClients.get(0)+"\n")){
                c = filteredClients.get(0);
                System.out.println("Chooce client to give transfer money"); 
                BankOperations.filter(1);
                c1 = filteredClients.get(0);
            } else{
                c1 = filteredClients.get(0);
                System.out.println("Choose client to receive transer money");
                BankOperations.filter(1);
                c = filteredClients.get(0);
            }
        } else{
            System.out.println("Choose client to receive transer money");
            BankOperations.filter(1);
            c = filteredClients.get(0);
            System.out.println("Chooce client to give transfer money"); 
            BankOperations.filter(1);
            c1 = filteredClients.get(0);
        }
        double amount = Utils.getUserDouble("Enter amount of money to transact.");
        if(Utils.getConfirmation("Transfer "+ amount + " money \nFrom  client \n" + c +"\nTo client\n"+c1+"\n")){
            c.addBalance(amount);
            c1.subBalance(amount);
            BankOperations.save();
        }
    }
    public static void save(){

        try{
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("Bank.ser"));
            out.writeObject(clients);
            out.close();
            ObjectOutputStream out2 = new ObjectOutputStream(new FileOutputStream("Config.ser"));
            out2.writeObject(BankClient.getNextId());
            out2.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    @SuppressWarnings("unchecked")
    public static void open(){
        if(new File("Bank.ser").isFile()){
            try{
                ObjectInputStream in = new ObjectInputStream(new FileInputStream("Bank.ser"));
                clients = (ArrayList)in.readObject();
                in.close();
                ObjectInputStream in2 = new ObjectInputStream(new FileInputStream("Config.ser"));
                BankClient.setNextId((Integer)in2.readObject());
                in2.close();
                System.out.println("System has been loaded");
            }catch(Exception e){
                System.out.println(e.getMessage());
            }
        }
    }
    private static void filterMenu(){
        System.out.println("[I]d");
        System.out.println("[N]ame");
        System.out.println("[S]urname");
        System.out.println("[P]esel");
        System.out.println("[A]ddress");
    }
}