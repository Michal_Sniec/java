import java.util.Scanner;

class Utils{
    public static boolean getConfirmation(String confirmationMessage){
        
        do{
            char input = getUserChar(confirmationMessage + " [Y]es [N]o");
            if(input == 'y')
                return true;
            if(input == 'n')
                return false;
        }while(true);
    }
    public static char getUserChar(String message){
        Scanner scanner = new Scanner(System.in);
        do{
            System.out.println(message);
            String input = scanner.nextLine();
            if(input.length() == 1)
                return input.toLowerCase().charAt(0);
            System.out.println("Expected single character got string instead");
        }while(true);
    }
    public static double getUserDouble(String message){
        Scanner scanner = new Scanner(System.in);
        do{
            System.out.println(message);
            try{
                String input = scanner.nextLine();
                if(input.indexOf(' ')==-1){
                    double retVal = Double.parseDouble(input.replace(",","."));
                    if(retVal * 100 %1 == 0 && retVal>0){
                        System.out.println(retVal);
                        return retVal;
                    } else{
                    System.out.println("Expected positive number in format *,2d or *.2d");
                    }
                } else{
                System.out.println("Expected number without spaces");
                }
            } catch (NumberFormatException e) {
                System.out.println("Expected number in format *,2d or *.2d");
            }
        }while(true);
    }
    public static String getUserString(String message){
        Scanner scanner = new Scanner(System.in);
        do{
            System.out.println(message);
            String input = scanner.nextLine();
            if(input.indexOf(' ') == -1 && input.matches("[a-zA-Z]+")){
                return input;
            }
            System.out.println("Expected single word without blank spaces containing only characters");
        }while(true);
    }
    public static String getPesel(String message){
        Scanner scanner = new Scanner(System.in);
        do{
            System.out.println(message);
            String input = scanner.nextLine();
            if((input.matches("[0-9]+") && input.length() == Config.maxPeselLength)){
                return input;
            }
            System.out.println("Expected "+ Config.maxPeselLength+ " numbers");
        }while(true);
    }
    public static String getUserLongText(String message){
        Scanner scanner = new Scanner(System.in);
        do{
            System.out.println(message);
            String input = scanner.nextLine();  
            if(input.matches("[a-zA-Z0-9\\/\\\\. -]+") && input.matches("[a-zA-Z].*")){
                return input;
            }
            System.out.println("Expected string begning from letter containing only characters,numbers,'.','/','\','-'");
        }while(true);
    }
    public static int getUserInt(String message){
        Scanner scanner = new Scanner(System.in);
        do{
            System.out.println(message);
            try{
                String input = scanner.nextLine();
                if(input.indexOf(' ')==-1){
                    int retVal = Integer.parseInt(input);
                    return retVal;
                }
                System.out.println("Exepcet number without spaces");
            } catch (NumberFormatException e) {
                System.out.println("Expected integer");
            }
        }while(true);
    }
}