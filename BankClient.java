import java.util.Queue;
import java.io.Serializable;
import java.util.PriorityQueue;

class BankClient implements Serializable{

    private int id;
    private String name;
    private String surname;
    private String pesel;
    private double balance;
    private String address;
    static int nextFreeId = 0;
    private Boolean confirmed;
    private static final long serialVersionUID = 1L;
    BankClient(String name, String  surname, String pesel, String address){
        id = nextFreeId;
        nextFreeId++;
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.address = address;
        balance = 0;
    }
    BankClient(int id, double balance, String name, String  surname, String pesel, String address){
        this.id = id;
        this.balance = balance;
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.address = address;
    }
    public void addBalance(double amount){
        this.balance += amount;
    }
    public void subBalance(double amount){
        this.balance-=amount;
    }
    public Boolean getConfirmation(){
        return confirmed;
    }
    public void setConfirmation(){
        confirmed = true;
    }
    public String toString(){
        return String.format(Config.getClientFormat(),id,balance,name + ' ' + surname, pesel, address);
    }
    public int getId(){
        return this.id;
    }
    public String getName(){
        return this.name;
    }
    public String getSurname(){
        return this.surname;
    }
    public String getPesel(){
        return this.pesel;
    }
    public String getAddress(){
        return this.address;
    }
    public static int getNextId(){
        return BankClient.nextFreeId;
    }
    public static void setNextId(int nextId){
        BankClient.nextFreeId = nextId;
    }
}