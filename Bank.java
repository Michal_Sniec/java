import java.util.ArrayList;
import java.util.Scanner;
class Bank{
    public static void main(String[] args){
        init();
        Boolean exitValue = true;
        do{
            displayMenu();
            char option = Utils.getUserChar("Select option from menu");
            switch(option){
                case 'a' :
                    BankOperations.addClient();
                break;
                case 'd' :
                    BankOperations.showAllClients();
                break;
                case 'f' :
                    BankOperations.filter(-1);
                break;
                case 't' :
                    BankOperations.transfer();
                break;
                case 'p' :
                    BankOperations.addBalance();
                break;
                case 's' :
                    BankOperations.subBalance();
                break;
                case 'r' :
                    BankOperations.removeClient();
                break;
                case 'w' :
                    BankOperations.save();
                break;
                case 'q' :
                    exitValue = false;
                break;
                case 'x' :
                    BankOperations.addTestClient();
                break;
                default:
                    System.out.println("No such option available options are charactes in square brackets");
                break;
            }
        }while(exitValue);
    }
    public static void displayMenu(){
        System.out.println("[D]isplay all clients");
        System.out.println("[A]dd client to bank");
        System.out.println("[R]emove bank client");
        System.out.println("[P]ay to given account");
        System.out.println("[S]ubtract from given account");
        System.out.println("[F]iletr bank clients");
        System.out.println("[T]ransfer between accounts");
        System.out.println("[W]rite clients to file");
    }
    public static void init(){
        BankOperations.open();
    }
}