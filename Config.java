import java.util.ArrayList;
import java.util.HashMap;
class Config{
    static int maxIdLength = 3;
    static int maxBalanceLength = 10;
    static int maxNameLength = 20;
    static int maxPeselLength = 11;
    static int maxAdresLength = 30;
    public static String getDelimiter(){
        return String.format(getFormat(),"","","","","").replaceAll(" ", "-");
    }
    public static String getTableHeader(){
        return String.format(getFormat(),"Id","Balance","Name surname", "Pesel", "Adress");
    }
    public static String getFormat(){
        return "|%1$-"+maxIdLength+"s|%2$-"+maxBalanceLength+"s|%3$-"+maxNameLength+"s|%4$-"+maxPeselLength+"s|%5$-"+maxAdresLength+"s|";
    }
    public static String getClientFormat(){
        return "|%1$-"+maxIdLength+"d|%2$-"+maxBalanceLength+".2f|%3$-"+maxNameLength+"s|%4$-"+maxPeselLength+"s|%5$-"+maxAdresLength+"s|";
    }
}